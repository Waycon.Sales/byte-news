Byte News foi criado em 2018.2 como projeto de conclusão da disciplina de HTML/CSS, portanto não foi utilizado nenhuma linguagem de programação ou plugin. Utilizou-se o framework Semantic UI para o desenvolvimento do projeto, o mesmo traz ao leitor notícias sobre esportes, games, filmes, o Brasil e o mundo. Como líder da equipe deleguei uma categoria para cada integrante, além de ter executado toda a arquitetura do site pelo celular facilitando assim sua responsividade. 

- EQUIPE: ANTONIO WAYCON, ANTONIO GABRIEL, GIOVANNI CAVALCANTE, MARCIO DUARTE, RAIMUNDO NETO.
